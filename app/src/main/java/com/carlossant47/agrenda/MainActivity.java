package com.carlossant47.agrenda;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private final ArrayList<Contacto> listContacto = new ArrayList<>();
    private EditText txtNombre;
    private EditText txtTel1;
    private EditText txtTel2;
    private EditText txtDomicilio;
    private EditText txtNotas;
    private CheckBox cbFavorito;
    private Button btnGuardar;
    final int READ_BLOCK_SIZE = 100;
    private Button btnLimpiar;
    private Button btnLista;
    private Button btnSalir;
    Contacto savedContent;
    int savedIndex;
    int index = 1;
    private boolean isEdit = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txtNombre = findViewById(R.id.txtNombre);
        txtTel1 = findViewById(R.id.txtTel1);
        txtTel2 = findViewById(R.id.txtTel2);
        txtDomicilio = findViewById(R.id.txtDomicilio);
        txtNotas = findViewById(R.id.txtNotas);
        cbFavorito = findViewById(R.id.cbFavorito);
        btnGuardar = findViewById(R.id.btnGuardar);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnLista = findViewById(R.id.btnLista);
        btnSalir = findViewById(R.id.btnSalir);
        btnGuardar.setOnClickListener(this.btnGuardarAction());
        btnLista.setOnClickListener(this.btnListarAction());
        btnLimpiar.setOnClickListener(this.btnLimpiarAction());
        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        try {
            loadContacts();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private View.OnClickListener btnListarAction()
    {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
                Intent intent = new Intent(MainActivity.this, ListaActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("contactos", listContacto);
                intent.putExtras(bundle);
                startActivityForResult(intent, 0);
            }
        };
    }

    private View.OnClickListener btnLimpiarAction()
    {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        };
    }
    private void limpiar(){
        txtDomicilio.setText("");
        txtNombre.setText("");
        txtTel1.setText("");
        txtTel2.setText("");
        txtNotas.setText("");
        cbFavorito.setChecked(false);
        savedContent = null;
        isEdit = false;
    }

    private View.OnClickListener btnGuardarAction()
    {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validar())
                {
                    Contacto contacto = new Contacto();
                    contacto.setNombre(getText(txtNombre));
                    contacto.setDireccion(getText(txtDomicilio));
                    contacto.setNotss(getText(txtNotas));
                    contacto.setTelefono(getText(txtTel1));
                    contacto.setTelefono2(getText(txtTel2));
                    contacto.setFavorito(cbFavorito.isChecked());
                    if(!isEdit) //si no voy a editar
                    {
                        //agregi un contacto
                        //el id el index que impieza en 1
                        //solo hara esto en agrefar
                        contacto.setId(index);
                        listContacto.add(contacto);
                        index++;
                    }
                    else{
                        contacto.setId(savedContent.getId());
                        editContact(contacto);
                        isEdit = false;
                        savedContent = null;
                    }

                    limpiar();
                    Toast.makeText(getApplicationContext(), R.string.mensaje,
                            Toast.LENGTH_SHORT).show();
                }
                else
                    Toast.makeText(getApplicationContext(), R.string.msgerror,
                            Toast.LENGTH_SHORT).show();
            }
        };
    }

    private boolean validar(){
        if(validarCampo(txtDomicilio) || validarCampo(txtNombre) ||
                validarCampo(txtNotas) || validarCampo(txtTel1) || validarCampo(txtTel2))
            return false;
        else
            return true;
    }


    private boolean validarCampo(EditText txt)
    {
        return txt.getText().toString().matches("");
    }

    private String getText(EditText txt)
    {
        return txt.getText().toString();
    }

    private void editContact(Contacto contacto)
    {
        for (int x = 0; x < this.listContacto.size(); x++)
        {
            if(this.listContacto.get(x).getId() == contacto.getId())
            {
                this.listContacto.set(x, contacto);
            }
        }
    }

    @Override
    protected void onActivityResult(int res, int result, Intent intent){
        if(intent != null)
        {
            Bundle bundle = intent.getExtras();
            int typeAction = bundle.getInt(getString(R.string.action));
            //regreso un entero, si es 0 solo sustituye la lista si es 1 es editar y sustituye los
            // los elementos de la lista por si las MOSCAS jajajajaj
            this.listContacto.clear();
            this.listContacto.addAll((ArrayList<Contacto>)
                    bundle.getSerializable(getString(R.string.listContactos)));
            //aunque sea editar o te salgas del listactivity siempre mandara la lista de regreso
            //por si se elimino y se quiere editar
            if(typeAction == 1)
            {
                savedContent = (Contacto) bundle.getSerializable("contacto");
                savedIndex = bundle.getInt("index");
                txtNombre.setText(savedContent.getNombre());
                txtNotas.setText(savedContent.getNotss());
                txtTel1.setText(savedContent.getTelefono());
                txtTel2.setText(savedContent.getTelefono2());
                txtDomicilio.setText(savedContent.getDireccion());
                cbFavorito.setChecked(savedContent.isFavorito());
                isEdit = true;
            }

        }
    }

    @Override
    public void onStop () {
//do your stuff here
        Log.e("MSG", "Close applicacion");
        savedContact();
        super.onStop();
    }

    public void loadContacts() throws IOException, JSONException {
        FileInputStream fin = openFileInput("contacts.json");
        InputStreamReader InputRead= new InputStreamReader(fin);

        char[] inputBuffer= new char[READ_BLOCK_SIZE];
        String s="";
        int charRead;

        while ((charRead=InputRead.read(inputBuffer))>0) {
            // char to string conversion
            String readstring=String.copyValueOf(inputBuffer,0,charRead);
            s +=readstring;
        }
        InputRead.close();
        if(s.length() == 0)
        {
            Log.e("MSG", "Archivo no disponible");
        }
        else{
            JSONObject object = new JSONObject(s);
            this.index = object.getInt("lastId");
            JSONArray contactosJSON = object.getJSONArray("contactos");
            listContacto.clear();
            for (int x = 0; x < contactosJSON.length(); x++)
            {
                Contacto contacto = new Contacto();
                contacto.setId(contactosJSON.getJSONObject(x).getInt("id"));
                contacto.setNombre(contactosJSON.getJSONObject(x).getString("nombre"));
                contacto.setTelefono(contactosJSON.getJSONObject(x).getString("telefono1"));
                contacto.setTelefono2(contactosJSON.getJSONObject(x).getString("telefono2"));
                contacto.setNotss(contactosJSON.getJSONObject(x).getString("notas"));
                contacto.setDireccion(contactosJSON.getJSONObject(x).getString("domicilio"));
                contacto.setFavorito(contactosJSON.getJSONObject(x).getBoolean("favorito"));
                listContacto.add(contacto);
            }
        }
        Log.e("MSG", s);

    }

    public void savedContact()
    {
        try {
            JSONObject todo = new JSONObject();
            todo.put("lastId", index);
            JSONArray con = new JSONArray();

            for (int x = 0; x < listContacto.size(); x++)
            {
                JSONObject object1 = new JSONObject();
                object1.put("id", listContacto.get(x).getId());
                object1.put("nombre", listContacto.get(x).getNombre());
                object1.put("telefono1", listContacto.get(x).getTelefono());
                object1.put("telefono2", listContacto.get(x).getTelefono2());
                object1.put("domicilio", listContacto.get(x).getDireccion());
                object1.put("notas", listContacto.get(x).getNotss());
                object1.put("favorito", listContacto.get(x).isFavorito());
                con.put(object1);
            }
            todo.put("contactos", con);
            Log.e("data", todo.toString());
            FileOutputStream fOut = openFileOutput("contacts.json", Context.MODE_PRIVATE);
            fOut.write(todo.toString().getBytes());
            fOut.close();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
